from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog
from PyQt5 import QtCore, QtGui, QtWidgets, QtSerialPort
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtMultimedia import *
from PyQt5.QtMultimediaWidgets import *
from PyQt5 import uic
import sys
import os
import shutil
import time
from datetime import datetime, date

#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#[name]
    #Check_Class
#...
#end_[name]
#______________________________________________________________


class Ui_Form(object):
    def setupUi(self, Form):
        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        #[ui]
            
            # QtDesigner
        Form.setObjectName("Form")
        Form.resize(1151, 675)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Form.sizePolicy().hasHeightForWidth())
        Form.setSizePolicy(sizePolicy)
        self.groupBox = QtWidgets.QGroupBox(Form)
        self.groupBox.setGeometry(QtCore.QRect(20, 20, 271, 101))
        self.groupBox.setObjectName("groupBox")
        self.connect = QtWidgets.QPushButton(self.groupBox)
        self.connect.setGeometry(QtCore.QRect(10, 60, 75, 23))
        self.connect.setObjectName("connect")
        self.nameport1 = QtWidgets.QComboBox(self.groupBox)
        self.nameport1.setGeometry(QtCore.QRect(10, 20, 69, 22))
        self.nameport1.setObjectName("nameport1")
        self.nameport2 = QtWidgets.QComboBox(self.groupBox)
        self.nameport2.setGeometry(QtCore.QRect(90, 20, 69, 22))
        self.nameport2.setObjectName("nameport2")
        self.disconnect = QtWidgets.QPushButton(self.groupBox)
        self.disconnect.setGeometry(QtCore.QRect(100, 60, 75, 23))
        self.disconnect.setObjectName("disconnect")
        self.baudrate = QtWidgets.QComboBox(self.groupBox)
        self.baudrate.setGeometry(QtCore.QRect(190, 20, 69, 22))
        self.baudrate.setObjectName("baudrate")
        self.rescan = QtWidgets.QPushButton(self.groupBox)
        self.rescan.setGeometry(QtCore.QRect(204, 60, 51, 23))
        self.rescan.setObjectName("rescan")
        self.groupBox_2 = QtWidgets.QGroupBox(Form)
        self.groupBox_2.setGeometry(QtCore.QRect(20, 130, 271, 141))
        self.groupBox_2.setObjectName("groupBox_2")
        self.data1 = QtWidgets.QLabel(self.groupBox_2)
        self.data1.setGeometry(QtCore.QRect(20, 30, 221, 16))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.data1.setFont(font)
        self.data1.setObjectName("data1")
        self.data2 = QtWidgets.QLabel(self.groupBox_2)
        self.data2.setGeometry(QtCore.QRect(20, 90, 221, 16))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.data2.setFont(font)
        self.data2.setObjectName("data2")
        self.cam = QtWidgets.QLabel(Form)
        self.cam.setGeometry(QtCore.QRect(320, 30, 291, 231))
        self.cam.setStyleSheet("background-color: rgb(0, 0, 0);")
        self.cam.setText("")
        self.cam.setObjectName("cam")
        self.pic1 = QtWidgets.QLabel(Form)
        self.pic1.setGeometry(QtCore.QRect(20, 290, 281, 201))
        self.pic1.setStyleSheet("background-color: rgb(0, 0, 0);")
        self.pic1.setText("")
        self.pic1.setObjectName("pic1")
        self.pic2 = QtWidgets.QLabel(Form)
        self.pic2.setGeometry(QtCore.QRect(320, 290, 281, 201))
        self.pic2.setStyleSheet("background-color: rgb(0, 0, 0);")
        self.pic2.setText("")
        self.pic2.setObjectName("pic2")
        self.groupBox_4 = QtWidgets.QGroupBox(Form)
        self.groupBox_4.setEnabled(True)
        self.groupBox_4.setGeometry(QtCore.QRect(620, 40, 231, 461))
        self.groupBox_4.setTitle("")
        self.groupBox_4.setObjectName("groupBox_4")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.groupBox_4)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 20, 211, 401))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.DKTV_ID = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.DKTV_ID.setFont(font)
        self.DKTV_ID.setObjectName("DKTV_ID")
        self.verticalLayout.addWidget(self.DKTV_ID)
        self.data1_14 = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.data1_14.setFont(font)
        self.data1_14.setObjectName("data1_14")
        self.verticalLayout.addWidget(self.data1_14)
        self.DKTV_ten = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.DKTV_ten.setFont(font)
        self.DKTV_ten.setObjectName("DKTV_ten")
        self.verticalLayout.addWidget(self.DKTV_ten)
        self.data1_12 = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.data1_12.setFont(font)
        self.data1_12.setObjectName("data1_12")
        self.verticalLayout.addWidget(self.data1_12)
        self.DKTV_namsinh = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.DKTV_namsinh.setFont(font)
        self.DKTV_namsinh.setObjectName("DKTV_namsinh")
        self.verticalLayout.addWidget(self.DKTV_namsinh)
        self.data1_13 = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.data1_13.setFont(font)
        self.data1_13.setObjectName("data1_13")
        self.verticalLayout.addWidget(self.data1_13)
        self.DKTV_sdt = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.DKTV_sdt.setFont(font)
        self.DKTV_sdt.setObjectName("DKTV_sdt")
        self.verticalLayout.addWidget(self.DKTV_sdt)
        self.data1_9 = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.data1_9.setFont(font)
        self.data1_9.setObjectName("data1_9")
        self.verticalLayout.addWidget(self.data1_9)
        self.DKTV_diachi = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.DKTV_diachi.setFont(font)
        self.DKTV_diachi.setObjectName("DKTV_diachi")
        self.verticalLayout.addWidget(self.DKTV_diachi)
        self.data1_11 = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.data1_11.setFont(font)
        self.data1_11.setObjectName("data1_11")
        self.verticalLayout.addWidget(self.data1_11)
        self.DKTV_ngaydangky = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.DKTV_ngaydangky.setFont(font)
        self.DKTV_ngaydangky.setInputMethodHints(QtCore.Qt.ImhNone)
        self.DKTV_ngaydangky.setObjectName("DKTV_ngaydangky")
        self.verticalLayout.addWidget(self.DKTV_ngaydangky)
        self.data1_10 = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.data1_10.setFont(font)
        self.data1_10.setObjectName("data1_10")
        self.verticalLayout.addWidget(self.data1_10)
        self.DKTV_thoihan = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.DKTV_thoihan.setFont(font)
        self.DKTV_thoihan.setObjectName("DKTV_thoihan")
        self.verticalLayout.addWidget(self.DKTV_thoihan)
        self.DKTV_dangky = QtWidgets.QPushButton(self.groupBox_4)
        self.DKTV_dangky.setGeometry(QtCore.QRect(10, 430, 75, 23))
        self.DKTV_dangky.setObjectName("DKTV_dangky")
        self.DKTV_checkbox = QtWidgets.QCheckBox(Form)
        self.DKTV_checkbox.setGeometry(QtCore.QRect(620, 10, 181, 31))
        self.DKTV_checkbox.setObjectName("DKTV_checkbox")
        self.groupBox_3 = QtWidgets.QGroupBox(Form)
        self.groupBox_3.setGeometry(QtCore.QRect(860, 20, 281, 481))
        self.groupBox_3.setObjectName("groupBox_3")
        self.gridLayoutWidget = QtWidgets.QWidget(self.groupBox_3)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 20, 259, 411))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label_6 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 5, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 1, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 5, 1, 1, 1)
        self.GT_unit1 = QtWidgets.QCheckBox(self.gridLayoutWidget)
        self.GT_unit1.setObjectName("GT_unit1")
        self.gridLayout.addWidget(self.GT_unit1, 4, 0, 1, 3)
        self.GT_txt0 = QtWidgets.QTextEdit(self.gridLayoutWidget)
        self.GT_txt0.setObjectName("GT_txt0")
        self.gridLayout.addWidget(self.GT_txt0, 2, 0, 1, 3)
        self.GT_unit0 = QtWidgets.QCheckBox(self.gridLayoutWidget)
        self.GT_unit0.setObjectName("GT_unit0")
        self.gridLayout.addWidget(self.GT_unit0, 0, 0, 1, 3)
        self.GT_txt1 = QtWidgets.QTextEdit(self.gridLayoutWidget)
        self.GT_txt1.setObjectName("GT_txt1")
        self.gridLayout.addWidget(self.GT_txt1, 8, 0, 1, 3)
        self.label_4 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 5, 2, 1, 1)
        self.label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 2, 1, 1)
        self.GT_txt2 = QtWidgets.QLineEdit(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.GT_txt2.setFont(font)
        self.GT_txt2.setObjectName("GT_txt2")
        self.gridLayout.addWidget(self.GT_txt2, 9, 2, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_7.setObjectName("label_7")
        self.gridLayout.addWidget(self.label_7, 9, 0, 1, 2)
        self.GT_luu = QtWidgets.QPushButton(self.groupBox_3)
        self.GT_luu.setGeometry(QtCore.QRect(10, 450, 75, 23))
        self.GT_luu.setObjectName("GT_luu")
        self.data1_2 = QtWidgets.QLabel(Form)
        self.data1_2.setGeometry(QtCore.QRect(30, 530, 71, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.data1_2.setFont(font)
        self.data1_2.setObjectName("data1_2")
        self.data1_3 = QtWidgets.QLabel(Form)
        self.data1_3.setGeometry(QtCore.QRect(160, 530, 71, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.data1_3.setFont(font)
        self.data1_3.setObjectName("data1_3")
        self.data1_4 = QtWidgets.QLabel(Form)
        self.data1_4.setGeometry(QtCore.QRect(300, 530, 51, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.data1_4.setFont(font)
        self.data1_4.setObjectName("data1_4")
        self.DT_ngay = QtWidgets.QComboBox(Form)
        self.DT_ngay.setGeometry(QtCore.QRect(90, 540, 61, 22))
        self.DT_ngay.setObjectName("DT_ngay")
        self.DT_thang = QtWidgets.QComboBox(Form)
        self.DT_thang.setGeometry(QtCore.QRect(230, 540, 61, 22))
        self.DT_thang.setObjectName("DT_thang")
        self.DT_nam = QtWidgets.QLineEdit(Form)
        self.DT_nam.setGeometry(QtCore.QRect(360, 540, 61, 22))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.DT_nam.setFont(font)
        self.DT_nam.setObjectName("DT_nam")
        self.data1_5 = QtWidgets.QLabel(Form)
        self.data1_5.setGeometry(QtCore.QRect(30, 580, 171, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.data1_5.setFont(font)
        self.data1_5.setObjectName("data1_5")
        self.DT_xem_chitiet = QtWidgets.QPushButton(Form)
        self.DT_xem_chitiet.setGeometry(QtCore.QRect(210, 590, 75, 23))
        self.DT_xem_chitiet.setObjectName("DT_xem_chitiet")
        self.DT_xem_ngay = QtWidgets.QPushButton(Form)
        self.DT_xem_ngay.setGeometry(QtCore.QRect(440, 530, 161, 23))
        self.DT_xem_ngay.setObjectName("DT_xem_ngay")
        self.DT_xem_thang = QtWidgets.QPushButton(Form)
        self.DT_xem_thang.setGeometry(QtCore.QRect(440, 570, 161, 23))
        self.DT_xem_thang.setObjectName("DT_xem_thang")
        self.DT_xem_nam = QtWidgets.QPushButton(Form)
        self.DT_xem_nam.setGeometry(QtCore.QRect(440, 610, 161, 23))
        self.DT_xem_nam.setObjectName("DT_xem_nam")
        self.showDTngay = QtWidgets.QLabel(Form)
        self.showDTngay.setGeometry(QtCore.QRect(620, 530, 91, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.showDTngay.setFont(font)
        self.showDTngay.setObjectName("showDTngay")
        self.showDTnam = QtWidgets.QLabel(Form)
        self.showDTnam.setGeometry(QtCore.QRect(620, 610, 91, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.showDTnam.setFont(font)
        self.showDTnam.setObjectName("showDTnam")
        self.showDTthang = QtWidgets.QLabel(Form)
        self.showDTthang.setGeometry(QtCore.QRect(620, 570, 91, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.showDTthang.setFont(font)
        self.showDTthang.setObjectName("showDTthang")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        #end_[ui]
        #______________________________________________________________

        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        #[camera]
            
            #get list camera
        self.available_cameras = QCameraInfo.availableCameras()

            # if no camera found
        if not self.available_cameras:
            sys.exit()
            # path to save
        self.save_path = "./pic"

            # creat camera view object
        self.viewfinder = QCameraViewfinder(Form)
        self.viewfinder.setGeometry(QtCore.QRect(320, 30, 291, 231))
        self.select_camera(0) # default camera

            # timer to show image
        self.timer1 = QtCore.QTimer()
        self.timer1.timeout.connect(self.Update1)

        self.timer2 = QtCore.QTimer()
        self.timer2.timeout.connect(self.Update2)

            # scale label
        self.pic1.setScaledContents(True)
        self.pic2.setScaledContents(True)

        #end_[camera]
        #______________________________________________________________

        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        #[Serial]
            
            # Select Port
        self.scanPort()
            
            # Port
        self.port1 = QtSerialPort.QSerialPort(
            readyRead=self.receive
        )
        self.port2 = QtSerialPort.QSerialPort(
            readyRead=self.receive
        )

            # clicked
        self.connect.clicked.connect(self.openPort)
        self.disconnect.clicked.connect(self.closePort)
        self.rescan.clicked.connect(self.scanPort)

            # ID - money
        self.IDcard = '00000000000'
        self.BienSo = '37P-4322'
        self.money = 0

        #end_[Serial]
        #______________________________________________________________


        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        #[DKTV]
            #Check_Class
        self.DKTV_checkbox.stateChanged.connect(self.checkbox)
        self.checkbox()
        self.DKTV_ngaydangky.setText(time.strftime("%d-%m-%Y"))
        self.DKTV_dangky.clicked.connect(self.dangky)
        #end_[DKTV]
        #______________________________________________________________

        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        #[Gt]
            #Check_Class
        self.GT_unit0.stateChanged.connect(self.GTcheck)
        self.GT_unit1.stateChanged.connect(self.GTcheck)
        self.GTcheck()
        self.GTload()
        self.GT_luu.clicked.connect(self.GTluu)
        #end_[GT]
        #______________________________________________________________

        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        #[Doanh Thu]
            # add ngày
        self.DT_ngay.addItems(['01','02','03','04','05','06','07','08','09','10',
                               '11','12','13','14','15','16','17','18','19','20',
                               '21','22','23','24','25','26','27','28','29','30','31'])
            # add tháng
        self.DT_thang.addItems(['01','02','03','04','05','06','07','08','09','10','11','12'])
            # show doanh thu ngày - tháng - năm
        self.DT_xem_ngay.clicked.connect(self.Show_ngay)
        self.DT_xem_thang.clicked.connect(self.Show_thang)
        self.DT_xem_nam.clicked.connect(self.Show_nam)
        self.DT_xem_chitiet.clicked.connect(self.Show_chitiet)
        #end_[Doanh Thu]
        #______________________________________________________________

    #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    #[program]

        # ui
    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "COM and CAM"))
        self.groupBox.setTitle(_translate("Form", "Serial"))
        self.connect.setText(_translate("Form", "connect"))
        self.disconnect.setText(_translate("Form", "disconnect"))
        self.rescan.setText(_translate("Form", "rescan"))
        self.groupBox_2.setTitle(_translate("Form", "Data"))
        self.data1.setText(_translate("Form", "Data1"))
        self.data2.setText(_translate("Form", "Data2"))
        self.DKTV_ID.setText(_translate("Form", "ID: 00000000001"))
        self.data1_14.setText(_translate("Form", "Họ tên"))
        self.DKTV_ten.setText(_translate("Form", "Nguyễn Anh Minh"))
        self.data1_12.setText(_translate("Form", "Ngày tháng năm sinh"))
        self.DKTV_namsinh.setText(_translate("Form", "24-03-1997"))
        self.data1_13.setText(_translate("Form", "Số điện thoại"))
        self.DKTV_sdt.setText(_translate("Form", "0968391088"))
        self.data1_9.setText(_translate("Form", "Địa chỉ"))
        self.DKTV_diachi.setText(_translate("Form", "Gò Vấp, Hồ Chí Minh"))
        self.data1_11.setText(_translate("Form", "Ngày đăng ký*"))
        self.DKTV_ngaydangky.setText(_translate("Form", "03-10-2021"))
        self.data1_10.setText(_translate("Form", "Thời hạn (ngày)*"))
        self.DKTV_thoihan.setText(_translate("Form", "180"))
        self.DKTV_dangky.setText(_translate("Form", "Đăng ký"))
        self.DKTV_checkbox.setText(_translate("Form", "đăng ký thẻ thành viên"))
        self.groupBox_3.setTitle(_translate("Form", "Giá tiền"))
        self.label_6.setText(_translate("Form", "Giờ"))
        self.label_2.setText(_translate("Form", "Phút"))
        self.label_5.setText(_translate("Form", "Phút"))
        self.GT_unit1.setText(_translate("Form", "Giá trên 1 giờ"))
        self.GT_txt0.setHtml(_translate("Form", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">1\t29\t6</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">2\t29\t10</p></body></html>"))
        self.GT_unit0.setText(_translate("Form", "Giá cố định"))
        self.GT_txt1.setHtml(_translate("Form", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">3\t29\t15</p></body></html>"))
        self.label_4.setText(_translate("Form", "Giá"))
        self.label.setText(_translate("Form", "Giờ"))
        self.label_3.setText(_translate("Form", "Giá"))
        self.GT_txt2.setText(_translate("Form", "20"))
        self.label_7.setText(_translate("Form", "Giá trên 1 giờ kể từ thời điểm cuối"))
        self.GT_luu.setText(_translate("Form", "Lưu"))
        self.data1_2.setText(_translate("Form", "Ngày:"))
        self.data1_3.setText(_translate("Form", "Tháng:"))
        self.data1_4.setText(_translate("Form", "Năm:"))
        self.DT_nam.setText(_translate("Form", "2021"))
        self.data1_5.setText(_translate("Form", "Lịch Sử Chi Tiết:"))
        self.DT_xem_chitiet.setText(_translate("Form", "Xem"))
        self.DT_xem_ngay.setText(_translate("Form", "Tổng Doanh Thu ngày đã chọn"))
        self.DT_xem_thang.setText(_translate("Form", "Tổng Doanh Thu tháng đã chọn"))
        self.DT_xem_nam.setText(_translate("Form", "Tổng Doanh Thu năm đã chọn"))
        self.showDTngay.setText(_translate("Form", "0.000đ"))
        self.showDTnam.setText(_translate("Form", "0.000đ"))
        self.showDTthang.setText(_translate("Form", "0.000đ"))
        
        # select_camera
    def select_camera(self,i):
        self.camera = QCamera(self.available_cameras[i])
        self.camera.setViewfinder(self.viewfinder) 
        self.camera.setCaptureMode(QCamera.CaptureStillImage)
        self.camera.error.connect(lambda: self.alert(self.camera.errorString()))
            #capture
        self.capture = QCameraImageCapture(self.camera)
        self.current_camera_name = self.available_cameras[i].description()

        # scan Port
    def scanPort(self):
            # COM
        self.nameport1.clear()
        self.nameport2.clear()
        for info in QtSerialPort.QSerialPortInfo.availablePorts():
            self.nameport1.addItem(info.portName())
            self.nameport2.addItem(info.portName())
            # Baud
        self.baudrate.clear()
        for baudrate in QtSerialPort.QSerialPortInfo.standardBaudRates():
            self.baudrate.addItem(str(baudrate), baudrate)      

        # capture Port1
    def cap1(self):
        self.save_string = self.save_path + "/" + "%s.jpg" % (self.IDcard)
        print(self.save_string)
        self.capture.capture(os.path.join(self.save_string))
        self.timer1.start(500)

        # update image Port1
    def Update1(self):
        self.pic1.setPixmap(QtGui.QPixmap(self.save_string))
        self.timer1.stop()

        # capture Port2
    def cap2(self):
        self.save_string = self.save_path + "/" + "%s.jpg" % (self.IDcard)
        #self.capture.capture(os.path.join(self.save_string))
        self.timer2.start(500)

        # update image Port2
    def Update2(self):
        self.pic2.setPixmap(QtGui.QPixmap(self.save_string))
        self.timer2.stop()

        #openPort
    def openPort(self):
        self.port1.close()
        self.port2.close()
        self.port1.setPortName(self.nameport1.currentText())
        self.port1.setBaudRate(self.baudrate.currentData())
        self.port1.open(QtCore.QIODevice.ReadWrite)
        self.port2.setPortName(self.nameport2.currentText())
        self.port2.setBaudRate(self.baudrate.currentData())
        self.port2.open(QtCore.QIODevice.ReadWrite)
        self.camera.start()
        self.TEST() #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        #closePort
    def closePort(self):
        self.port1.close()
        self.port2.close()
        self.camera.stop()

        # rx_Port 
    def receive(self):
        if self.port1.canReadLine():
            self.IDcard = self.port1.readLine().data().decode()
            self.IDcard = self.IDcard.replace("\n","")
            self.IDcard = self.IDcard.replace("\r","")
            if self.DKTV_checkbox.isChecked() == True:
                self.DKTV_ID.setText("ID: " + self.IDcard)
            else:
                self.data1.setText(self.IDcard)
                self.port1.write(self.IDcard.encode())
                self.data_input()

        if self.port2.canReadLine():
            self.IDcard = self.port2.readLine().data().decode()
            self.IDcard = self.IDcard.replace("\n","")
            self.IDcard = self.IDcard.replace("\r","")
            if self.DKTV_checkbox.isChecked() == True:
                self.DKTV_ID.setText("ID: " + self.IDcard)
            else:
                self.data2.setText(self.IDcard)
                self.port2.write(self.IDcard.encode())
                self.data_output()

        # data input
    def data_input(self):
            # information
        here_path = os.path.dirname(os.path.realpath('__file__'))
        datenow = time.strftime("%d-%m-%Y")
        timenow = time.strftime("%H:%M:%S")
        data_path = "MetaData/"
        date_path = ''  #datenow + "/"
        id_path = self.IDcard + "/"
        file_txt_name = "information.txt"
        file_img_name = self.IDcard + ".jpg"
            # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Days/" + date_path + id_path)):
            os.makedirs(os.path.join(here_path,data_path + "Days/" + date_path + id_path))
            # check file txt
        if not os.path.exists(os.path.join(here_path,data_path + "Days/" + date_path + id_path + file_txt_name)):
            with open(os.path.join(here_path,data_path + "Days/" + date_path + id_path + file_txt_name), 'w') as newfile: 
                newfile.close()
        else:
            open(os.path.join(here_path,data_path + "Days/" + date_path + id_path + file_txt_name), 'w').close()
            # import txt
        f = open(os.path.join(here_path,data_path + "Days/" + date_path + id_path + file_txt_name),'a')
        f.write( "[GO IN]\t\t" + self.IDcard + "\n" + datenow + "\t" + timenow)
        f.close()
            # import img
        self.save_string = here_path + "/"  + data_path + "Days/" + date_path + id_path + file_img_name
        self.capture.capture(os.path.join(self.save_string))
        print(self.save_string)
        self.timer1.start(500)
            # save data to history
                # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Memory")):
            os.makedirs(os.path.join(here_path,data_path + "Memory"))
                # check file txt
        if not os.path.exists(os.path.join(here_path,data_path + "Memory/"+ datenow +".txt")):
            with open(os.path.join(here_path,data_path + "Memory/"+ datenow +".txt"), 'w') as newfile: 
                newfile.close()
                # write data
        f = open(os.path.join(here_path,data_path + "Memory/"+ datenow +".txt"),'a')
        f.write( self.IDcard + "\t" + timenow + "\n")
        f.close()

        # data output
    def data_output(self):
            # information
        here_path = os.path.dirname(os.path.realpath('__file__'))
        datenow = time.strftime("%d-%m-%Y")
        timenow = time.strftime("%H:%M:%S")
        monthnow = time.strftime("%m-%Y")
        yearnow = time.strftime("%Y")
        data_path = "MetaData/"
        date_path = datenow + "/"
        id_path = self.IDcard + "/"
        file_txt_name = "information.txt"
        file_img_name = self.IDcard + ".jpg"
            # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Days/" + self.IDcard)):
            print("Khong co data cho ID")
            return
            # show img
        self.save_string = here_path + "/"  + data_path + "Days/" + id_path + file_img_name
        self.pic2.setPixmap(QtGui.QPixmap(self.save_string))
            # get data from information.txt to calculate money
                # datetime
        f = open(os.path.join(here_path,data_path + "Days/" + id_path + file_txt_name),'r')
        data = f.readlines()
        f.close()
        infor = data[1].split() # get day and time GO IN
            # check IDcard normal or month
        ismonth = True
        if not os.path.exists(os.path.join(here_path,data_path  + "Month/" + file_txt_name)):
            print("Không có Dữ liệu vé tháng")
            ismonth = False
            LoaiVe = 'vãng lai'
            print("nomal")
        else:
            f = open(os.path.join(here_path,data_path  + "Month/" + file_txt_name),'r')
            data_month = f.readlines()
            f.close()
            for x in range(len(data_month)):
                if x == 0:
                    continue
                data_month[x] = data_month[x].replace("\n","")
                infor_month = data_month[x].split()
                id_month = int(infor_month[0])
                date_month = infor_month[1]
                day_month = int(infor_month[2])
                myIDcard = int(self.IDcard)
                check = datetime.strptime(datenow, "%d-%m-%Y") - datetime.strptime(date_month, "%d-%m-%Y")
                LoaiVe = 'NAN'
                if myIDcard == id_month and check.days <= day_month:
                    ismonth = True
                    print("month")
                    LoaiVe = 'vé tháng'
                    self.money = 0
                    #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< add code here
                    break
                else:
                    ismonth = False
                    LoaiVe = 'vãng lai'
                    print("nomal")
        if not ismonth:
                    # day
            FMT = '%d-%m-%Y'
            infor_day = datetime.strptime(datenow, FMT) - datetime.strptime(infor[0], FMT)
            print(infor_day.days)
                    # time
            FMT = '%H:%M:%S'
            infor_time = datetime.strptime(timenow, FMT) - datetime.strptime(infor[1], FMT)
            print(infor_time)
            h, m, s = str(infor_time).split(':')
                    # money
            f = open(os.path.join(here_path,data_path + "Money/" + file_txt_name),'r')
            moneystr = f.readlines()
            f.close()
            for x in range(len(moneystr) - 2):
                moneystr[x] = moneystr[x].replace("\n","")
                infor_money = moneystr[x].split()
                BG_unit = int(infor_money[0])
                BG_gio = int(infor_money[1])
                BG_phut = int(infor_money[2])
                BG_gia = int(infor_money[3])
                ngay = infor_day.days
                gio = int(h) + ngay*24
                phut = int(m)
                if(BG_unit == 2 and gio >= BG_gio):
                    self.money = BG_gia*gio
                    print(self.money)
                    break
                if(BG_unit == 0 and gio == BG_gio):
                    if(phut <= BG_phut):
                        self.money = BG_gia
                        print(self.money)
                        break
                if(BG_unit == 0 and gio < BG_gio):
                    self.money = BG_gia
                    print(self.money)
                    break
                if(BG_unit == 1 and gio == BG_gio):
                    if(phut <= BG_phut):
                        self.money = BG_gia*gio
                        print(self.money)
                        break
                if(BG_unit == 1 and gio < BG_gio):
                    self.money = BG_gia*gio
                    print(self.money)
                    break
                #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< add code here
            # save data to history
        if not os.path.exists(os.path.join(here_path,data_path + "History/" + datenow + ".csv")):
            with open(os.path.join(here_path,data_path + "History/" + datenow + ".csv"), 'a',encoding='utf16') as newfile: 
                newfile.write('stt\tmã thẻ\tngày vào\tgiờ vào\tbiển số\tkhách\tngày ra\tgiờ ra\t$ (nghin đồng)\n')
                newfile.close()
        f = open(os.path.join(here_path,data_path + "History/" + datenow + ".csv"), 'r',encoding='utf16')
        STT = f.readlines()
        f.close()
            #for x in range(len(data_month)):
        f = open(os.path.join(here_path,data_path + "History/" + datenow + ".csv"), 'a',encoding='utf16')
        f.write(str(len(STT)) + '\t_' + self.IDcard + '\t' + infor[0] + '\t' + infor[1] + '\t' + self.BienSo
                + '\t' + LoaiVe + '\t' + datenow + '\t' + timenow + '\t' + str(self.money) + '\n')
        f.close()
        #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< add code here
            # delete memory
        shutil.rmtree(os.path.join(here_path,data_path + "Days/" + self.IDcard))
        #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< add code here
            # update total sales Day > Month > Year          
        if not os.path.exists(os.path.join(here_path,data_path + "Total Sales/Day/" + datenow + "/TotalSales.txt")):
            if not os.path.exists(os.path.join(here_path,data_path + "Total Sales/Day/" + datenow)):
                os.makedirs(os.path.join(here_path,data_path + "Total Sales/Day/" + datenow))
            with open(os.path.join(here_path,data_path + "Total Sales/Day/" + datenow + "/TotalSales.txt"), 'w') as newfile: 
                newfile.write(str(self.money))
                newfile.close()
        else:
            f = open(os.path.join(here_path,data_path + "Total Sales/Day/" + datenow + "/TotalSales.txt"), 'r')
            data = f.readline()
            f.close()
            f = open(os.path.join(here_path,data_path + "Total Sales/Day/" + datenow + "/TotalSales.txt"), 'w')
            write = self.money + int(data)
            f.write(str(write))
            f.close()
            
        if not os.path.exists(os.path.join(here_path,data_path + "Total Sales/Month/" + monthnow + "/TotalSales.txt")):
            if not os.path.exists(os.path.join(here_path,data_path + "Total Sales/Month/" + monthnow)):
                os.makedirs(os.path.join(here_path,data_path + "Total Sales/Month/" + monthnow))
            with open(os.path.join(here_path,data_path + "Total Sales/Month/" + monthnow + "/TotalSales.txt"), 'w') as newfile: 
                newfile.write(str(self.money))
                newfile.close()
        else:
            f = open(os.path.join(here_path,data_path + "Total Sales/Month/" + monthnow + "/TotalSales.txt"), 'r')
            data = f.readline()
            f.close()
            f = open(os.path.join(here_path,data_path + "Total Sales/Month/" + monthnow + "/TotalSales.txt"), 'w')
            write = self.money + int(data)
            f.write(str(write))
            f.close()

        if not os.path.exists(os.path.join(here_path,data_path + "Total Sales/Year/" + yearnow + "/TotalSales.txt")):
            if not os.path.exists(os.path.join(here_path,data_path + "Total Sales/Year/" + yearnow)):
                os.makedirs(os.path.join(here_path,data_path + "Total Sales/Year/" + yearnow))
            with open(os.path.join(here_path,data_path + "Total Sales/Year/" + yearnow + "/TotalSales.txt"), 'w') as newfile: 
                newfile.write(str(self.money))
                newfile.close()
        else:
            f = open(os.path.join(here_path,data_path + "Total Sales/Year/" + yearnow + "/TotalSales.txt"), 'r')
            data = f.readline()
            f.close()
            f = open(os.path.join(here_path,data_path + "Total Sales/Year/" + yearnow + "/TotalSales.txt"), 'w')
            write = self.money + int(data)
            f.write(str(write))
            f.close()
        #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< add code here 

        # DKTV
    def checkbox(self):
        if self.DKTV_checkbox.isChecked() == True:
            self.groupBox_4.setEnabled(True)
        else:
            self.groupBox_4.setDisabled(True)

    def dangky(self):
        here_path = os.path.dirname(os.path.realpath('__file__'))
        data_path = "MetaData/"
        print(self.DKTV_ten.text() + "\n" + self.DKTV_namsinh.text() + "\n" + self.DKTV_sdt.text() + "\n" 
              + self.DKTV_diachi.text() + "\n" + self.DKTV_ngaydangky.text() + "\n" + self.DKTV_thoihan.text())
            # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Month")):
                os.makedirs(os.path.join(here_path,data_path + "Month"))
            # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Month/ID")):
                os.makedirs(os.path.join(here_path,data_path + "Month/ID"))
            # check ID.txt
        if not os.path.exists(os.path.join(here_path,data_path + "Month/ID/"+ self.IDcard + ".txt")):
            with open(os.path.join(here_path,data_path + "Month/ID/"+ self.IDcard + ".txt"), 'w') as newfile: 
                newfile.close()
            # check sig.txt
        if not os.path.exists(os.path.join(here_path,data_path + "Month/sig.txt")):
            with open(os.path.join(here_path,data_path + "Month/sig.txt"), 'w') as newfile: 
                newfile.close()
            # check information.txt
        if not os.path.exists(os.path.join(here_path,data_path + "Month/information.txt")):
            with open(os.path.join(here_path,data_path + "Month/information.txt"), 'w') as newfile: 
                newfile.write("[ID]\t\t[date]\t\t[days]\n")
                newfile.close()
        f = open(os.path.join(here_path,data_path + "Month/sig.txt"),'a',encoding='utf8')
        f.write("[" + self.IDcard + "]\t{\n" 
                + "\t\t\t" + self.DKTV_ten.text() + "\n" 
                + "\t\t\t" + self.DKTV_namsinh.text() + "\n"
                + "\t\t\t" + self.DKTV_sdt.text() + "\n"
                + "\t\t\t" + self.DKTV_diachi.text() + "\n"
                + "\t\t\t" + self.DKTV_ngaydangky.text() + "\n"
                + "\t\t\t" + self.DKTV_thoihan.text() + "\n" 
                + "\t\t}\n")
        f.close()
        f = open(os.path.join(here_path,data_path + "Month/information.txt"),'a',encoding='utf8')
        f.write(self.IDcard + "\t" + self.DKTV_ngaydangky.text() + "\t" + self.DKTV_thoihan.text() + "\n")
        f.close()
        f = open(os.path.join(here_path,data_path + "Month/ID/"+ self.IDcard + ".txt"), 'w',encoding='utf8')
        f.write( self.DKTV_ten.text() + "\n" 
                + self.DKTV_namsinh.text() + "\n"
                + self.DKTV_sdt.text() + "\n"
                + self.DKTV_diachi.text() + "\n"
                + self.DKTV_ngaydangky.text() + "\n"
                + self.DKTV_thoihan.text())
        f.close()

        #GT
    def GTcheck(self):
        if self.GT_unit0.isChecked() == True:
            self.GT_txt0.setEnabled(True)
        else:
            self.GT_txt0.setDisabled(True)

        if self.GT_unit1.isChecked() == True:
            self.GT_txt1.setEnabled(True)
            self.GT_txt2.setEnabled(True)
        else:
            self.GT_txt1.setDisabled(True)
            self.GT_txt2.setDisabled(True)

        if self.GT_unit0.isChecked() == True or self.GT_unit1.isChecked() == True:
            self.GT_luu.setEnabled(True)
        else:
            self.GT_luu.setDisabled(True)

    def GTload(self):
        here_path = os.path.dirname(os.path.realpath('__file__'))
        data_path = "MetaData/"
            # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Money")):
                os.makedirs(os.path.join(here_path,data_path + "Money"))
            # check information.txt
        if not os.path.exists(os.path.join(here_path,data_path + "Money/information.txt")):
            with open(os.path.join(here_path,data_path + "Money/information.txt"), 'w') as newfile: 
                newfile.write("0\t01\t2\t6\n" 
                              + "0\t02\t29\t10\n" 
                              + "1\t03\t29\t15\n" 
                              + "2\t03\t30\t20\n" 
                              + "unit\ttime[h]\ttime[m]\tmoney\n" 
                              + "[unit: 0 money; 1 money per time; 2 money per time and time start at... ]")
                newfile.close()

        f = open(os.path.join(here_path,data_path + "Money/information.txt"),'r')
        moneystr = f.readlines()
        f.close()
        string_txt0 =''
        string_txt1 =''
        for x in range(len(moneystr) - 2):
            moneystr[x] = moneystr[x].replace("\n","")
            infor_money = moneystr[x].split()
            GT_unit = int(infor_money[0])
            if(GT_unit == 2):
                self.GT_txt2.setText(infor_money[3])
            elif(GT_unit == 0):
                string_txt0 += infor_money[1] + "\t" + infor_money[2] + "\t" + infor_money[3] + "\n"
            elif(GT_unit == 1):
                string_txt1 += infor_money[1] + "\t" + infor_money[2] + "\t" + infor_money[3] + "\n"
        self.GT_txt0.setText(string_txt0)
        self.GT_txt1.setText(string_txt1)
    
    def GTluu(self):
        here_path = os.path.dirname(os.path.realpath('__file__'))
        data_path = "MetaData/"
            # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Money")):
                os.makedirs(os.path.join(here_path,data_path + "Money"))
            # check information.txt
        if not os.path.exists(os.path.join(here_path,data_path + "Money/information.txt")):
            with open(os.path.join(here_path,data_path + "Money/information.txt"), 'w') as newfile: 
                newfile.write("0\t01\t2\t6\n" 
                              + "0\t02\t29\t10\n" 
                              + "1\t03\t29\t15\n" 
                              + "2\t03\t30\t20\n" 
                              + "unit\ttime[h]\ttime[m]\tmoney\n" 
                              + "[unit: 0 money; 1 money per time; 2 money per time and time start at... ]")
                newfile.close()
        f = open(os.path.join(here_path,data_path + "Money/information.txt"),'w',encoding='utf8')
        if self.GT_unit0.isChecked() == True:
            data_txt0 = self.GT_txt0.toPlainText()
            line_txt0 = data_txt0.split('\n')
            for x in range(len(line_txt0)):
                f.write("0\t" + line_txt0[x] + '\n')#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        if self.GT_unit1.isChecked() == True:
            data_txt1 = self.GT_txt1.toPlainText()
            line_txt1 = data_txt1.split('\n')
            line_txt2 = ''
            for x in range(len(line_txt1)):
                f.write("1\t" + line_txt1[x] + '\n')#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                line_txt2 = line_txt1[x].split('\t')
            f.write("2\t" + line_txt2[0] + "\txx\t" + self.GT_txt2.text() + '\n')#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        f.write("unit\ttime[h]\ttime[m]\tmoney\n"
                + "[unit: 0 money; 1 money per time; 2 money per time and time start at... ]")
        f.close()
        print("ok")
        pass
    
        # Doanh Thu
    def Show_ngay(self):
        here_path = os.path.dirname(os.path.realpath('__file__'))
        data_path = "MetaData/"
        folder_name = self.DT_ngay.currentText() + '-' + self.DT_thang.currentText() + '-' + self.DT_nam.text()
            # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Total Sales/Day/" + folder_name)):
            print("Khong co du lieu")
            return
        f = open(os.path.join(here_path,data_path + "Total Sales/Day/" + folder_name + "/TotalSales.txt"),'r')
        self.showDTngay.setText(f.read() + '.000đ')
        f.close()

    def Show_thang(self):
        here_path = os.path.dirname(os.path.realpath('__file__'))
        data_path = "MetaData/"
        folder_name = self.DT_thang.currentText() + '-' + self.DT_nam.text()
            # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Total Sales/Month/" + folder_name)):
            print("Khong co du lieu")
            return
        f = open(os.path.join(here_path,data_path + "Total Sales/Month/" + folder_name + "/TotalSales.txt"),'r')
        self.showDTthang.setText(f.read() + '.000đ')
        f.close()

    def Show_nam(self):
        here_path = os.path.dirname(os.path.realpath('__file__'))
        data_path = "MetaData/"
        folder_name = self.DT_nam.text()
            # check folder
        if not os.path.exists(os.path.join(here_path,data_path + "Total Sales/Year/" + folder_name)):
            print("Khong co du lieu")
            return
        f = open(os.path.join(here_path,data_path + "Total Sales/Year/" + folder_name + "/TotalSales.txt"),'r')
        self.showDTnam.setText(f.read() + '.000đ')
        f.close()

    def Show_chitiet(self):
        here_path = os.path.dirname(os.path.realpath('__file__'))
        data_path = "MetaData/"
        filename = self.DT_ngay.currentText() + '-' + self.DT_thang.currentText() + '-' + self.DT_nam.text()
        os.startfile(os.path.join(here_path,data_path + "History/" + filename + ".csv"))

    #end_[program]
    #______________________________________________________________

            #test
    def TEST(self):
        pass

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

