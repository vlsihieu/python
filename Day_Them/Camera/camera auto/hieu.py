from cryptography.fernet import Fernet
 
def encrypt(message: bytes, key: bytes) -> bytes:
    return Fernet(key).encrypt(message)
 
def decrypt(token: bytes, key: bytes) -> bytes:
    return Fernet(key).decrypt(token)
key = Fernet.generate_key()
message = 'John Doe'
a = encrypt(message.encode(), key)
print(a)


b = decrypt(a, key).decode()
print(b)