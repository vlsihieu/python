from pyzbar import pyzbar
import imutils
from imutils.video import VideoStream
import sys
from PIL import Image
import cv2
from datetime import datetime
import time
import csv
#____________________________________#
vs = VideoStream(src=0).start()
time.sleep(2.0)
now = datetime.now()
while True:
    frame = vs.read()
    frame = imutils.resize(frame, width=400)
    barcodes = pyzbar.decode(frame)
    for barcode in barcodes:
        (x, y, w, h) = barcode.rect
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        barcodeData = barcode.data.decode("utf-8")
        barcodeType = barcode.type
        text = "{}".format(barcodeData)
        cv2.putText(frame, '', (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
        if barcodeData:
            barcodeData = barcodeData.split(",")
            print("------------------------------")
            print("Ma don hang: " + barcodeData[0])
            print("Ten san pham: " + barcodeData[1])
            print("Ten nguoi nhan: " + barcodeData[2])
            print("Dia chi nhan hang: " + barcodeData[3])
            print("So dien thoai: " + barcodeData[4])
            print("Thoi gian quet ma QR: " + now.strftime("%d/%m/%Y %H:%M:%S"))
            userScanned = False
            with open('Database.csv') as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    if row["Ma don hang"] == barcodeData[0]:
                        userScanned = True
            if userScanned == False:
                with open('Database.csv', 'a') as csvfile:
                    fieldNames = ['Ma don hang', 'Ten san pham', 'Ten nguoi nhan', 'Dia chi', 'So dien thoai', 'Thoi gian quet ma QR']
                    writer = csv.DictWriter(csvfile, fieldnames=fieldNames)
                    writer.writerow({'Ma don hang': barcodeData[0], 'Ten san pham': barcodeData[1], 'Ten nguoi nhan': barcodeData[2], 'Dia chi': barcodeData[3], 'So dien thoai': barcodeData[4], 'Thoi gian quet ma QR': now.strftime("%d/%m/%Y %H:%M:%S")})
            kho = str(barcodeData[0])
            tensanpham = barcodeData[1]
            tennguoinhan = barcodeData[2]
            diachinhanhang = barcodeData[3]
            sodienthoai = barcodeData[4]
            if kho == str('HN1'):
                print("Di chuyen den kho 1")
                print("------------------------------")
            else :
                print("None")
            
    cv2.imshow("Quet ma", frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break
cv2.destroyAllWindows()
vs.stop()
        
