from PyQt5 import QtWidgets,uic
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QVBoxLayout
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon, QPixmap, QFont
from PyQt5.QtCore import QTimer, QTime, Qt
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from datetime import datetime
import time
import cv2
import sys
import serial
import os
from pyzbar import pyzbar
import csv
import imutils
from imutils.video import VideoStream
#---------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------
kho = ""
tensanpham = ""
tennguoinhan = ""
diachinhanhang = ""
sodienthoai = ""
now = datetime.now()
def Time():
    global kho, tensanpham, tennguoinhan, diachinhanhang, sodienthoai
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S %d/%m/%y")
    call.lb_time.setText(current_time)
    call.lb_time.setAlignment(QtCore.Qt.AlignCenter)#can chinh ve trung tam khung
    call.lb_kho.setAlignment(QtCore.Qt.AlignCenter)
    call.lb_kho.setText(kho)
    call.lb_tensanpham.setAlignment(QtCore.Qt.AlignCenter)
    call.lb_tensanpham.setText(tensanpham)
    call.lb_tennguoinhan.setAlignment(QtCore.Qt.AlignCenter)
    call.lb_tennguoinhan.setText(tennguoinhan)
    call.lb_diachi.setAlignment(QtCore.Qt.AlignCenter)
    call.lb_diachi.setText(diachinhanhang)
    call.lb_sdt.setAlignment(QtCore.Qt.AlignCenter)
    call.lb_sdt.setText(sodienthoai)
    
    
#------------------------------------------------------------------#
def Detect():
    global kho, tensanpham, tennguoinhan, diachinhanhang, sodienthoai
#------------------------------------------------------------------#
def thoat():
    call.close()
    exit(app.exec())
    sys.exit()
#------------------------------------------------------------------#
class Worker1(QThread): 
    def run(self):
        global kho, tensanpham, tennguoinhan, diachinhanhang, sodienthoai
        self.ThreadActive = True
        vs = VideoStream(src=0).start()
        time.sleep(2.0)
        now = datetime.now()
        while True:
            frame = vs.read()
            frame = imutils.resize(frame, width=240)
            barcodes = pyzbar.decode(frame)
            img = QImage(frame, frame.shape[1], frame.shape[0], QImage.Format_RGB888).rgbSwapped()
            pix = QPixmap.fromImage(img)
            call.lb_img.setPixmap(pix)
            for barcode in barcodes:
                (x, y, w, h) = barcode.rect
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                barcodeData = barcode.data.decode("utf-8")
                barcodeType = barcode.type
                text = "{}".format(barcodeData)
                cv2.putText(frame, '', (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                if barcodeData:
                    barcodeData = barcodeData.split(",")
                    print("------------------------------")
                    print("Ma don hang: " + barcodeData[0])
                    print("Ten san pham: " + barcodeData[1])
                    print("Ten nguoi nhan: " + barcodeData[2])
                    print("Dia chi nhan hang: " + barcodeData[3])
                    print("So dien thoai: " + barcodeData[4])
                    print("Thoi gian quet ma QR: " + now.strftime("%d/%m/%Y %H:%M:%S"))
                    userScanned = False
                    with open('Database.csv') as csvfile:
                        reader = csv.DictReader(csvfile)
                        for row in reader:
                            if row["Ma don hang"] == barcodeData[0]:
                                userScanned = True
                    if userScanned == False:
                        with open('Database.csv', 'a') as csvfile:
                            fieldNames = ['Ma don hang', 'Ten san pham', 'Ten nguoi nhan', 'Dia chi', 'So dien thoai', 'Thoi gian quet ma QR']
                            writer = csv.DictWriter(csvfile, fieldnames=fieldNames)
                            writer.writerow({'Ma don hang': barcodeData[0], 'Ten san pham': barcodeData[1], 'Ten nguoi nhan': barcodeData[2], 'Dia chi': barcodeData[3], 'So dien thoai': barcodeData[4], 'Thoi gian quet ma QR': now.strftime("%d/%m/%Y %H:%M:%S")})
                    kho = str(barcodeData[0])
                    tensanpham = barcodeData[1]
                    tennguoinhan = barcodeData[2]
                    diachinhanhang = barcodeData[3]
                    sodienthoai = barcodeData[4]
                    if kho == str('HN1'):
                        print("Di chuyen den kho 1")
                        print("------------------------------")
                    else :
                        print("None")
            key = cv2.waitKey(1) & 0xFF
            if key == ord("q"):
                break
        cv2.destroyAllWindows()
        vs.stop()
        
        
        
                
    def stop(self):
        self.ThreadActive = False
        self.quit()
#------------------------------------------------------------------#

app=QtWidgets.QApplication([])
call=uic.loadUi("AUTO.ui")
call.QUIT.clicked.connect(thoat)
call.CHAY.clicked.connect(Detect)
call.timer = QTimer()
call.timer.timeout.connect(Time)
call.timer.start(100)
call.Worker1 = Worker1()
call.Worker1.start()
call.show()
app.exec()