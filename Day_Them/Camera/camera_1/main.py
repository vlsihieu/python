from PyQt5 import QtWidgets, uic
import sys
import random
import math # thư viện toán học
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QComboBox, QHBoxLayout
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QFont
import os
from PyQt5 import QtCore, QtWidgets,QtGui
from PyQt5 import uic
import sys, time
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow,  QLineEdit, QApplication, QWidget, QAction, QTableWidget,QTableWidgetItem,QVBoxLayout,QMessageBox
from PyQt5.QtCore import Qt,pyqtSlot,QThread,pyqtSignal,QObject
from PyQt5.uic import loadUi
from threading import Thread
import sys
import time
import threading
from datetime import date
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot


from PyQt5 import QAxContainer
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QLineEdit, QApplication
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, QFileDialog
from PyQt5.QtGui import QIcon

import random

import subprocess

from PyQt5 import QtCore, QtGui, QtWidgets
import time
from PyQt5 import QtCore, QtGui, QtWidgets
import time
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QTableWidget, QTableWidgetItem, QPushButton, QHeaderView, QHBoxLayout, QVBoxLayout
from PyQt5.QtCore import QThread, Qt,QTimer
import pandas as pd # pip install pandas
from threading import Thread
import os
from PyQt5.QtWidgets import QApplication,QMainWindow, QMessageBox
import time

from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout
from PyQt5.QtGui import QPixmap
import sys
import cv2
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread
import numpy as np

from PyQt5 import QtWidgets,uic
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QVBoxLayout
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon, QPixmap, QFont
from PyQt5.QtCore import QTimer, QTime, Qt
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from datetime import datetime
import time
import cv2
import sys
import serial
import os
from pyzbar import pyzbar # thư viện qrcode
import csv
import imutils
from imutils.video import VideoStream

now = datetime.now()
dt_string = now.strftime("%d/%m/%Y ")
kho = ""
tensanpham = ""
tennguoinhan = ""
diachinhanhang = ""
sodienthoai = ""


# bài tập về nhà
# # viết 1 giao diện có 2 chức năng tự động 
# hàm tự tính tổng
# hàm tự ramdom
class VideoThread(QThread):
    # global kho, tensanpham, tennguoinhan, diachinhanhang, sodienthoai,text
    # thuộc tính thread 
    change_pixmap_signal = pyqtSignal(np.ndarray)
    signal_kho = pyqtSignal(str)
    signal_tensanpham = pyqtSignal(str)
    signal_tennguoinhan = pyqtSignal(str)
    signal_diachinhanhang = pyqtSignal(str)
    signal_sodienthoai = pyqtSignal(str)
    # any_signal = pyqtSignal(str)
    def run(self):
        # capture from web cam
        cap = cv2.VideoCapture(0)
        while True:
            ret, cv_img = cap.read()
            if ret:
                self.change_pixmap_signal.emit(cv_img)

                barcodes = pyzbar.decode(cv_img)
                for barcode in barcodes:
                    (x, y, w, h) = barcode.rect
                    # print(x,y,w,h)
                    cv2.rectangle(cv_img, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    barcode_text = barcode.data.decode("utf-8")
                    barcodeType = barcode.type
                    text = "{}".format(barcode_text)
                    cv2.putText(cv_img, text, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                    # print(text)
                    if text:
                        barcode_text = text.split(",") # cắt chuỗi -> list 
                        print(barcode_text) # in
                        print("------------------------------")
                        print("Ma don hang: " + barcode_text[0])
                        print("Ten san pham: " + barcode_text[1])
                        print("Ten nguoi nhan: " + barcode_text[2])
                        print("Dia chi nhan hang: " + barcode_text[3])
                        print("So dien thoai: " + barcode_text[4])
                        print("Thoi gian quet ma QR: " + now.strftime("%d/%m/%Y %H:%M:%S")) # google
                        userScanned = False
                        with open('Database.csv') as csvfile:
                            reader = csv.DictReader(csvfile)
                            for row in reader:
                                if row["Ma don hang"] == barcode_text[0]:
                                    userScanned = True
                        if userScanned == False:
                            with open('Database.csv', 'a') as csvfile:
                                fieldNames = ['Ma don hang', 'Ten san pham', 'Ten nguoi nhan', 'Dia chi', 'So dien thoai', 'Thoi gian quet ma QR']
                                writer = csv.DictWriter(csvfile, fieldnames=fieldNames)
                                writer.writerow({'Ma don hang': barcode_text[0], 'Ten san pham': barcode_text[1], 'Ten nguoi nhan': barcode_text[2], 'Dia chi': barcode_text[3], 'So dien thoai': barcode_text[4], 'Thoi gian quet ma QR': now.strftime("%d/%m/%Y %H:%M:%S")})
                        kho = str(barcode_text[0])
                        self.signal_kho.emit(kho) # class thread , có đối tượng signal_kho chưa giá trị kho
                        tensanpham = str(barcode_text[1])
                        self.signal_tensanpham.emit(tensanpham)
                        tennguoinhan = str(barcode_text[2])
                        self.signal_tennguoinhan.emit(tennguoinhan)
                        diachinhanhang = str(barcode_text[3])
                        self.signal_diachinhanhang.emit(diachinhanhang)
                        sodienthoai = str(barcode_text[4])
                        self.signal_sodienthoai.emit(sodienthoai)
                        if kho == str('HN1'):
                            print("Di chuyen den kho 1")
                            print("------------------------------")
                        else :
                            print("None")


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        
        uic.loadUi('AUTO.ui', self)
        self.disply_width = 281
        self.display_height = 321
        self.lb_img.resize(self.disply_width, self.display_height)
        self.lb_img.setScaledContents(True);

        self.thread = VideoThread()
        # connect its signal to the update_image slot
        self.thread.change_pixmap_signal.connect(self.update_image)
        #  đối tượng nó , kết nối hàm thực thi của nó .
        self.thread.signal_kho.connect(self.show_kho) # giá trị thuộc tính singal_kho trả về cho function show_kho
        self.thread.signal_tensanpham.connect(self.show_tensp)
        self.thread.signal_tennguoinhan.connect(self.show_tennguoinhan)
        self.thread.signal_diachinhanhang.connect(self.show_diachikhachhang)
        self.thread.signal_sodienthoai.connect(self.show_sdt)
        # start the thread
        self.thread.start()

        self.lb_time.setText(str(dt_string))
        # tự tìm hiểu
        self.qTimer2 = QTimer()
        self.qTimer2.setInterval(1)
        self.qTimer2.timeout.connect(self.DateTime)
        self.qTimer2.start() 

    def DateTime(self):
        Dt = QDateTime.currentDateTime()
        clock = Dt.toString("hh:mm:ss")
        self.lb_time_2.setText(str(clock))     
    @pyqtSlot(str)
    def show_kho(self,kho):
        self.lb_kho.setText(kho)
    @pyqtSlot(str)
    def show_tensp(self,tensanpham):
        self.lb_tensanpham.setText(tensanpham)
    @pyqtSlot(str)
    def show_tennguoinhan(self,tennguoinhan):
        self.lb_tennguoinhan.setText(tennguoinhan)
    @pyqtSlot(str)
    def show_diachikhachhang(self,diachinhanhang):
        self.lb_diachi.setText(diachinhanhang)
    @pyqtSlot(str)
    def show_sdt(self,sodienthoai):
        self.lb_sdt.setText(sodienthoai)
    @pyqtSlot(np.ndarray)
    def update_image(self, cv_img):
        """Updates the image_label with a new opencv image"""
        qt_img = self.convert_cv_qt(cv_img)


        self.lb_img.setPixmap(qt_img)
    
    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_Qt_format.scaled(self.disply_width, self.display_height, Qt.KeepAspectRatio)
    # def time(self):


        return QPixmap.fromImage(p)
    # def read_qr(self, cv_img):

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()

    # _worker = Worker1()
    # _worker.data.connect(main.worker_data_callback)
    # _worker.start()

    main.show()
    sys.exit(app.exec_())
