from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout
from PyQt5.QtGui import QPixmap
import sys
import cv2
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread
import numpy as np
from PyQt5 import QtWidgets, uic
import sys
import random
import math # thư viện toán học
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QComboBox, QHBoxLayout
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QFont
import os
from PyQt5 import QtCore, QtWidgets,QtGui
from PyQt5 import uic
import sys, time
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow,  QLineEdit, QApplication, QWidget, QAction, QTableWidget,QTableWidgetItem,QVBoxLayout,QMessageBox
from PyQt5.QtCore import Qt,pyqtSlot,QThread,pyqtSignal,QObject
from PyQt5.uic import loadUi
from threading import Thread
import sys
import time
import threading
from datetime import date
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot


from PyQt5 import QAxContainer
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QLineEdit, QApplication
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, QFileDialog
from PyQt5.QtGui import QIcon

import random

import subprocess

from PyQt5 import QtCore, QtGui, QtWidgets
import time
from PyQt5 import QtCore, QtGui, QtWidgets
import time
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QTableWidget, QTableWidgetItem, QPushButton, QHeaderView, QHBoxLayout, QVBoxLayout
from PyQt5.QtCore import QThread, Qt,QTimer
import pandas as pd # pip install pandas
from threading import Thread
import os
from PyQt5.QtWidgets import QApplication,QMainWindow, QMessageBox
import time

from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout
from PyQt5.QtGui import QPixmap
import sys
import cv2
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread
import numpy as np

from PyQt5 import QtWidgets,uic
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QVBoxLayout
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon, QPixmap, QFont
from PyQt5.QtCore import QTimer, QTime, Qt
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from datetime import datetime
import time
import cv2
import sys
import serial
import os
from pyzbar import pyzbar
import csv
import imutils
from imutils.video import VideoStream
# class Qthread này liên kết tín hiệu cv_img
# class Qthread , a dùng signal. ko dùng đc biến toàn cục
class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)
    # signal_kho = pyqtSignal(str)
    # signal_tensanpham = pyqtSignal(str)
    # signal_tennguoinhan = pyqtSignal(str)
    # signal_diachinhanhang = pyqtSignal(str)
    # signal_sodienthoai = pyqtSignal(str)
    def run(self):
        # capture from web cam
        cap = cv2.VideoCapture(0) # 0 là cam máy, 1 cam ngoài
        while True:
            ret, cv_img = cap.read()
            # tồn tại frame thì đọc gửi dữ liệu xuống xử lý
            if ret:
                self.change_pixmap_signal.emit(cv_img)
                barcodes = pyzbar.decode(cv_img) # đọc qrcode mã hóa
                for barcode in barcodes:
                    (x, y, w, h) = barcode.rect # hình
                    cv2.rectangle(cv_img, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    barcode_text = barcode.data.decode("utf-8")
                    barcodeType = barcode.type
                    text = "{}".format(barcode_text)
                    

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        
        uic.loadUi('AUTO.ui', self)
        self.disply_width = 281
        self.display_height = 321
        self.lb_img.resize(self.disply_width, self.display_height)
        self.lb_img.setScaledContents(True);

        self.thread = VideoThread()
        # connect its signal to the update_image slot
        self.thread.change_pixmap_signal.connect(self.update_image)
        self.thread.start()



    @pyqtSlot(np.ndarray)
    def update_image(self, cv_img):
        """Updates the lb_img with a new opencv image"""
        qt_img = self.convert_cv_qt(cv_img) # kỹ hơn a tìm hiểu hàm này làm gì
        self.lb_img.setPixmap(qt_img)


    
    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        #  bước tiền xử lý opencv
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        # kích thước khung hình
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        # hình ảnh truyền vào lable a convert . format  sang chuẩn Qt 
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        # scaled lại tỷ lệ khung hình
        p = convert_to_Qt_format.scaled(self.disply_width, self.display_height, Qt.KeepAspectRatio)
        # trong QT HIỂN THỊ pixmap
        return QPixmap.fromImage(p)
    
if __name__=="__main__":
    app = QApplication(sys.argv)
    a = MainWindow()
    a.show()
    sys.exit(app.exec_())