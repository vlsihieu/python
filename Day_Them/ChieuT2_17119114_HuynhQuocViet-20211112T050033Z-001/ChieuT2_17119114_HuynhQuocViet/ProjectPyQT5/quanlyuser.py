# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'quanlyuser.ui'
#
# Created by: PyQt5 UI code generator 5.15.1
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets
import sqlite3
conn = sqlite3.connect("mydata.db")
conn.execute('''CREATE TABLE IF NOT EXISTS user_lists(
            USER INT PRIMARY KEY NOT NULL,
            PASSWORD TEXT NOT NULL)''')
conn.commit()

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(528, 414)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(100, 20, 281, 41))
        font = QtGui.QFont()
        font.setFamily("Georgia")
        font.setPointSize(26)
        self.label.setFont(font)
        self.label.setTabletTracking(False)
        self.label.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label.setTextFormat(QtCore.Qt.AutoText)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(30, 90, 61, 41))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(16)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(30, 130, 61, 41))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(16)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.ten = QtWidgets.QLineEdit(self.centralwidget)
        self.ten.setGeometry(QtCore.QRect(80, 100, 221, 20))
        self.ten.setText("")
        self.ten.setObjectName("ten")
        self.matkhau = QtWidgets.QLineEdit(self.centralwidget)
        self.matkhau.setGeometry(QtCore.QRect(80, 140, 221, 20))
        self.matkhau.setObjectName("matkhau")
        self.pushButton_them = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_them.setGeometry(QtCore.QRect(360, 100, 81, 31))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(14)
        self.pushButton_them.setFont(font)
        self.pushButton_them.setObjectName("pushButton_them")
        self.pushButton_xoa = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_xoa.setGeometry(QtCore.QRect(360, 140, 81, 31))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(14)
        self.pushButton_xoa.setFont(font)
        self.pushButton_xoa.setObjectName("pushButton_xoa")
        self.pushButton_sua = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_sua.setGeometry(QtCore.QRect(360, 220, 81, 31))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(14)
        self.pushButton_sua.setFont(font)
        self.pushButton_sua.setObjectName("pushButton_sua")
        self.pushButton_tim = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_tim.setGeometry(QtCore.QRect(360, 180, 81, 31))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(14)
        self.pushButton_tim.setFont(font)
        self.pushButton_tim.setObjectName("pushButton_tim")
        self.pushButton_xem = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_xem.setGeometry(QtCore.QRect(360, 260, 81, 31))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(14)
        self.pushButton_xem.setFont(font)
        self.pushButton_xem.setObjectName("pushButton_xem")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(50, 190, 121, 41))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        font.setPointSize(18)
        self.label_4.setFont(font)
        self.label_4.setTabletTracking(False)
        self.label_4.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label_4.setTextFormat(QtCore.Qt.AutoText)
        self.label_4.setObjectName("label_4")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(50, 230, 261, 141))
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setRowCount(5)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 528, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        ### kết nối button tới các def ###
        self.pushButton_them.clicked.connect(self.funcion_add)
        self.pushButton_xoa.clicked.connect(self.funcion_deleted)
        self.pushButton_sua.clicked.connect(self.funcion_edit)
        self.pushButton_tim.clicked.connect(self.funcion_search)
        self.pushButton_xem.clicked.connect(self.display) 
        ###
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "QUẢN LÝ USER"))
        self.label_2.setText(_translate("MainWindow", "User"))
        self.label_3.setText(_translate("MainWindow", "Pass"))
        self.pushButton_them.setText(_translate("MainWindow", "Thêm"))
        self.pushButton_xoa.setText(_translate("MainWindow", "Xóa"))
        self.pushButton_sua.setText(_translate("MainWindow", "Sửa"))
        self.pushButton_tim.setText(_translate("MainWindow", "Search"))
        self.pushButton_xem.setText(_translate("MainWindow", "Xem"))
        self.label_4.setText(_translate("MainWindow", "Kết quả:"))
        item = self.tableWidget.verticalHeaderItem(0)
        item.setText(_translate("MainWindow", "STT"))
        item = self.tableWidget.verticalHeaderItem(1)
        item.setText(_translate("MainWindow", "1"))
        item = self.tableWidget.verticalHeaderItem(2)
        item.setText(_translate("MainWindow", "2"))
        item = self.tableWidget.verticalHeaderItem(3)
        item.setText(_translate("MainWindow", "3"))
        item = self.tableWidget.verticalHeaderItem(4)
        item.setText(_translate("MainWindow", "4"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "USER"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "PASS"))
    ######
    def display(self):
        dbase = sqlite3.connect('mydata.db')    
        query = "SELECT * FROM user_lists"
        result = dbase.execute(query)
        self.tableWidget.setRowCount(0)
        for row_number, row_data in enumerate(result):
            self.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))
        dbase.commit()

    def funcion_add(self):
        dbase = sqlite3.connect('mydata.db') 
        str_user=self.ten.text()
        str_pass=self.matkhau.text()   
        dbase.execute(''' INSERT INTO user_lists(USER,PASSWORD) 
                    VALUES(?,?)
        ''',(str_user,str_pass))
        # dbase.execute("INSERT INTO employee_records(USER,PASSWORD)  VALUES(?,?)",(str_user,str_pass)) 
        
        query = "SELECT * FROM user_lists"
        result = dbase.execute(query)
        self.tableWidget.setRowCount(0)
        for row_number, row_data in enumerate(result):
            self.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))
        dbase.commit()

    def funcion_deleted(self):
        dbase = sqlite3.connect('mydata.db') 
        str_user=self.ten.text()
        str_pass=self.matkhau.text()  
        dbase.execute(''' DELETE FROM user_lists WHERE USER = ? 
        ''',[str_user])   
        # dbase.execute("DELETE FROM employee_records WHERE USER = ?",[str_user])
        query = "SELECT * FROM user_lists"
        result = dbase.execute(query)
        self.tableWidget.setRowCount(0)
        for row_number, row_data in enumerate(result):
            self.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))
        dbase.commit()

    def funcion_edit(self):
        dbase = sqlite3.connect('mydata.db') 
        str_user=self.ten.text()
        str_pass=self.matkhau.text()  
        dbase.execute("UPDATE user_lists SET PASSWORD = ?  WHERE USER = ? ",(str_pass,str_user))
        
        query = "SELECT * FROM user_lists"
        result = dbase.execute(query)
        self.tableWidget.setRowCount(0)
        for row_number, row_data in enumerate(result):
            self.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))
        dbase.commit()

    def funcion_search(self):
        str_user = self.ten.text()
        dbase = sqlite3.connect("mydata.db")
        result = dbase.execute("SELECT *FROM user_lists WHERE USER = ?",[str_user])
        self.tableWidget.setRowCount(0)
        for row_number, row_data in enumerate(result):
            self.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))
        dbase.commit()    

    ######
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
