class Xe:
    def __init__(self):
        print("Hàm khởi tạo lớp cha")
 
class XeDap(Xe):
    def __init__(self):
    	# Xe.__init__(self) 1 cách gọi hàm khởi tạo có đối số kế thừa
    	super().__init__()
    	print("Hàm khởi tạo lớp con")
 
# Cách dùng
d = XeDap()
# print(d.)