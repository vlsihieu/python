from PyQt5 import QtWidgets, uic
import sys
import random
import math # thư viện toán học
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QComboBox, QHBoxLayout
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QFont

from PyQt5 import QtCore, QtWidgets,QtGui
from PyQt5 import uic
import sys, time
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow,  QLineEdit, QApplication, QWidget, QAction, QTableWidget,QTableWidgetItem,QVBoxLayout,QMessageBox
from PyQt5.QtCore import Qt,pyqtSlot,QThread,pyqtSignal,QObject
from PyQt5.uic import loadUi
from threading import Thread
import sys
import time
import psutil
import threading
from pheptoan import MyClass
# em dung Qtbread , này có ưu điểm gì so với threading python

# cho công việc 1 tính tổng
# class doanhthu(QtCore.QThread):
#     def __init__(self, parent = None):
#         super(doanhthu, self).__init__(parent)
#     DT=pyqtSignal(int) # tín hiệu mình kết nối để thread 
#     def run(self):
#         while 1:
#             DT=psutil.cpu_percent() 
#             # print(DT)
#             self.DT.emit(DT) # connect tín hiệu đó lại
#             time.sleep(1)
# # công việc 2 . random 
# # QMuxt cũng là 1 thead
# class dongia(QtCore.QThread):# Qrunable
#     def __init__(self, parent = None):
#         super(dongia, self).__init__(parent)
#     DG=pyqtSignal(int)
#     def run(self):
#         while 1:
#             DG= time.time()
#             # DG = psutil.cpu_percent()
#             self.DG.emit(DG)
#             time.sleep(1)

class Worker1(QtCore.QThread):
    data = QtCore.pyqtSignal(dict)

    def __init__(self, parent=None):
        super(Worker1, self).__init__(parent)
        self._stopped = True
        self._mutex = QtCore.QMutex()

    def stop(self):
        self._mutex.lock()
        self._stopped = True
        self._mutex.unlock()

    def run(self):
        self._stopped = False
        count = 0
        while True:
            if self._stopped:
                break
            self.sleep(1)
            data = {
                'message': 'running %d [%d]' % (count, QtCore.QThread.currentThreadId()),
                'time': QtCore.QTime.currentTime(),
                'items': [1, 2, 3],
                'progress': random.randrange(101)
            }
            self.data.emit(data)
            count+=1

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        
        #Load the UI Page
        # có nhiều cách load ui
        # đây là 1 cách load trực tiếp load thằng file.ui
        # khi a thay đổi ui a chỉ cần save thì tự thay đổi bên code ,
        # ->ko bị mất code mình viết
        # học 1 thời gian----> kỹ năng đọc hiểu code website -->ko hiểu print()
        uic.loadUi('buoi1.ui', self)
        self.pushButton_2.clicked.connect(self.radio_check)
        # self.pushButton.clicked.connect(self.start_worker_1)

        # combobox advance----------------
        self.model = QStandardItemModel()
        # dic key : value
        data = {
        'phòng A': ['1', '3', '5','7', '9', "hieu", '9.5'],
        'phòng B': ['2', '4', '6','8']
        }
        self.comboBox.setModel(self.model) # phòng
        self.comboBox_2.setModel(self.model)# số phòng
        # thêm dữ liệu
        for k, v in data.items():
            state = QStandardItem(k)
            print(v) # value
            print(k) # key
            print("State", state)
            self.model.appendRow(state) # thêm phần tử vào list
            for value in v:
                city = QStandardItem(value)
                print("city", city)
                state.appendRow(city)
        # connect function updatestatecombo
        self.comboBox_2.currentIndexChanged.connect(self.updateStateCombo)
        self.updateStateCombo(0)# hiển thị chỉ số đầu tiên

        # self.d_thu=doanhthu() # em gọi Qthead
        # self.d_thu.start() # em bắt đầu Qthead
        # self.d_thu.DT.connect(self.doanh_thu)

        # self.d_gia=dongia() # gọi Qthead tính random
        # self.d_gia.start() # bắt đầu thread 
        # self.d_gia.DG.connect(self.showradom)
        self.pushButton.clicked.connect(self.showradom)
        self.pushButton_3.clicked.connect(self.tinhtong)
        
    def showradom(self):
        # self.write(str(random.randint(3, 9)));setText show lên , lấy về self.lineEdit.text()
        self.lineEdit.setText(str(random.randint(3, 9))) # arrange 3->9     
        with open("example.txt", "a") as f:
            f.write(str(random.randint(3, 9)) + "\t")
        self.label_3.setText(self.lineEdit.text())

    def radio_check(self):
        if self.radioButton.isChecked() and self.checkBox.isChecked():
            self.lineEdit_2.setText("hiếu")
        elif self.radioButton.isChecked():
            self.lineEdit_2.setText("bạn dùng radio")
        elif self.checkBox.isChecked():
            self.lineEdit_2.setText("bạn dùng checkBox")
        else:
            self.lineEdit_2.clear()

    # upadate khi thay đổi combobox
    def updateStateCombo(self, index):
        indx = self.model.index(index, 0, self.comboBox_2.rootModelIndex())
        self.comboBox.setRootModelIndex(indx) # gg search
        self.comboBox.setCurrentIndex(0)

    def doanh_thu(self):
        a1 = int(self.a.text())
        b1 = int(self.b.text())
        doanh_thu = a1 + b1
        self.txt_kq.setText(str(doanh_thu))


    def tinhtong(self):
        # hieu = Myclass()
        hx = int(self.lineEdit_3.text())
        hy = int(self.lineEdit_4.text())
        self.label_2.setText(str(hx + hy))

    # def worker_data_callback(self, data):
    #     self.doanh_thu()
    # def worker_data_callback_1(self, data):
    #     self.showradom()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()

    # Form2 = QtWidgets.QWidget()
    hieu = MyClass()

    # _worker = Worker1()
    # _worker.data.connect(main.worker_data_callback)
    # _worker.start()

    # _worker1 = Worker1()
    # _worker1.data.connect(main.worker_data_callback_1)
    # _worker1.start()


    main.show()
    sys.exit(app.exec_())
