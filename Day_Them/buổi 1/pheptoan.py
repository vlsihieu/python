class MyClass(object):
    def __init__(self):
        self._foo ='' # thuộc tính
    # get phương thức
    def foo_get(self):
        return self._foo
    # set phương thức
    def foo_set(self, val):
        self._foo = val
        # return self._foo

    # lấy đối tượng của thằng Myclass ra
    x = property(foo_get, foo_set)
    y = property(foo_get, foo_set)
    
    def tinhtoan(self,x,y):
        return x + y

c = MyClass()
print(c.foo_set(5))
# print(c.foo_get())
# print(c.x)

# biến : name
# _name: protected dùng trong lớp kế thừa
# __name: private dùng lớp nội bộ
# name : public công cộng