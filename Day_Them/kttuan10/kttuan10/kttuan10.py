# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sale.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QTime, QTimer, QDateTime, QDate, QTime
from threading import Thread
import threading
import sqlite3
import mysql.connector
import time
import random

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="",
  database="kttuan10"
)
i = 0
k= 0
count = 0
mycursor = mydb.cursor()
mycursor.execute("SELECT COUNT(*) FROM dathang")
ids = mycursor.fetchone()[0]
print (ids)
time1 = 3
time2 = 3
class Worker1(QtCore.QThread):
    data = QtCore.pyqtSignal(dict)

    def __init__(self, parent=None):
        super(Worker1, self).__init__(parent)
        self._stopped = True
        self._mutex = QtCore.QMutex()

    def stop(self):
        self._mutex.lock()
        self._stopped = True
        self._mutex.unlock()

    def run(self):
        self._stopped = False
        count = 0
        #for count in range(1000):
        while True:
            if self._stopped:
                break
            self.sleep(6)
            data = {
                'message': 'running %d [%d]' % (
                    count, QtCore.QThread.currentThreadId()),
                'time': QtCore.QTime.currentTime(),
                'items': [1, 2, 3],
                'progress': random.randrange(101)
            }
            self.data.emit(data)
            count+=1
class Ui_MainWindow(object):
    def insertData(self):
        mycursor = mydb.cursor()
        mycursor.execute("SELECT COUNT(*) FROM dathang")
        ids = mycursor.fetchone()[0]
        id_user = int(int(ids) + 1)
        hoten = self.lineEdit.text()
        #email = self.email_lineEdit.text()
        sdt = self.lineEdit_2.text()
        if len(hoten) > 0 and len(sdt)>0:
            print("dat thanh cong")
            mycursor = mydb.cursor()
            mycursor.execute("INSERT INTO dathang (id,hoten,sdt)""VALUES('%d','%s','%d')" %(int(id_user),hoten,int(sdt)))
            mydb.commit()
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1099, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setGeometry(QtCore.QRect(260, 30, 20, 491))
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setGeometry(QtCore.QRect(570, 30, 20, 491))
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setGeometry(QtCore.QRect(1060, 30, 20, 491))
        self.line_3.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(40, 25, 181, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(340, 30, 181, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_2.setFont(font)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(610, 30, 451, 41))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_3.setFont(font)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setGeometry(QtCore.QRect(102, 100, 141, 31))
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setGeometry(QtCore.QRect(100, 170, 141, 31))
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(10, 100, 71, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(30, 180, 55, 16))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(334, 89, 121, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(330, 160, 121, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(370, 250, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(620, 110, 431, 411))
        self.tableWidget.setRowCount(1)
        self.tableWidget.setColumnCount(3)
        self.tableWidget.setObjectName("tableWidget")
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(330, 330, 101, 31))
        self.label_9.setObjectName("label_9")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setGeometry(QtCore.QRect(410, 335, 55, 21))
        self.label_10.setObjectName("label_10")
        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setGeometry(QtCore.QRect(410, 375, 55, 21))
        self.label_11.setObjectName("label_11")
        self.label_12 = QtWidgets.QLabel(self.centralwidget)
        self.label_12.setGeometry(QtCore.QRect(330, 370, 101, 31))
        self.label_12.setObjectName("label_12")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(80, 250, 141, 81))
        self.pushButton.setObjectName("pushButton")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1099, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.pushButton.clicked.connect(self.insertData)
        self.pushButton.clicked.connect(self.t2)
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "MUA HÀNG"))
        self.label_2.setText(_translate("MainWindow", "MUA HÀNG"))
        self.label_3.setText(_translate("MainWindow", "DANH SÁCH KHÁCH HÀNG ĐÃ MUA"))
        self.label_4.setText(_translate("MainWindow", "HỌ TÊN"))
        self.label_5.setText(_translate("MainWindow", "SĐT"))
        self.label_6.setText(_translate("MainWindow", "*"))
        self.label_7.setText(_translate("MainWindow", "*"))
        self.label_8.setText(_translate("MainWindow", "STATUS"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "ID"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "HOTEN"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "SDT"))
        self.label_9.setText(_translate("MainWindow", "ĐANG XỬ LÝ"))
        self.label_10.setText(_translate("MainWindow", "0s"))
        self.label_11.setText(_translate("MainWindow", "0s"))
        self.label_12.setText(_translate("MainWindow", "ĐANG GIAO"))
        self.pushButton.setText(_translate("MainWindow", "ĐẶT"))
    def t2(self):
        mycursor = mydb.cursor()
        mycursor.execute("SELECT * FROM damua")
        result = mycursor.fetchall()
        self.tableWidget.setRowCount(0)
        for row_number, row_data in enumerate(result):
            self.tableWidget.insertRow(row_number)
            for column_number, data in enumerate(row_data):
                self.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(str(data)))
        #connection.close()
        mydb.commit()
    def t1(self):
        global k, count
        #count = 6
        i = 0
        text3 = []
        if k > 0:
            count = 6
            mycursor = mydb.cursor()
            mycursor.execute("SELECT COUNT(*) FROM dathang")
            ids = mycursor.fetchone()[0]
            #if i < ids:
            #print(i)
            if ids > 0:
                #count = 6
                mycursor.execute("SELECT hoten FROM dathang where id ='%d'" %(int(i+1)))
                result = mycursor.fetchall()
                text = str(result[0][0])
                if ids > 1:
                    for j in range(ids - 1):
                        mycursor.execute("SELECT hoten FROM dathang where id ='%d'" %(int(i+2+j)))
                        result = mycursor.fetchall()
                        text3.append(str(result[0][0]))
                self.label_6.setText(text)
                mycursor.execute("SELECT sdt FROM dathang where id ='%d'" %(int(i+1)))
                result1 = mycursor.fetchall()
                text1 = str(result1[0][0])
                self.label_7.setText(text1)
                #mycursor.execute("SELECT timming FROM personal where id ='%d'" %(int(self.which+1)))
                mycursor = mydb.cursor()
                mycursor.execute("DELETE FROM dathang where id ='%s'"%(i+1))
                mydb.commit()
                print(text3)
                mycursor = mydb.cursor()
                if ids > 1:
                    for j in range(len(text3)):
                        
                        mycursor.execute("UPDATE dathang set id ='%d' where hoten = '%s'" %(int(j+1),text3[j]))
                mycursor.execute("SELECT COUNT(*) FROM damua")
                ids = mycursor.fetchone()[0]
                id_damua = int(int(ids) + 1)
                mycursor.execute("INSERT INTO damua (id,hoten,sdt)""VALUES('%d','%s','%d')" %(id_damua,text,result1[0][0]))
                mydb.commit()
            else:
                #i = ids
                count = 0
                self.label_7.setText(" ")
                self.label_6.setText(" ")
        else:
            count = 0
            k += 1

    def worker_started_callback(self):
        pass

    def worker_finished_callback(self):
        pass

    def worker_data_callback(self, data):
        self.t1()
        
    def worker_data_callback1(self, data):
        self.t2()
    def timer(self):
        global count
        if count > 3:
            st = str(count - 3) + "s"
            self.label_10.setText(st)
            self.label_11.setText("3s")
        else:
            st = str(count) + "s"
            self.label_10.setText("0s")
            self.label_11.setText(st)
        if count > 0:
            count -= 1

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    _serial_connected = False
    _serial_connecting = False
    _current_serial_ports = None
    _start_scan = False

    _worker = Worker1()
    _worker.data.connect(ui.worker_data_callback)
    _worker.start()

    _worker1 = Worker1()
    _worker1.data.connect(ui.worker_data_callback1)
    _worker1.start()

    timer = QTimer()
    timer.timeout.connect(ui.timer)
    timer.start(1000)
    sys.exit(app.exec_())

