from PyQt5 import QtWidgets, uic
import sys
import random 
import math # thư viện toán học 
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QComboBox, QHBoxLayout
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QFont
from PyQt5 import QtWidgets

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):

        super(MainWindow, self).__init__(*args, **kwargs)
        uic.loadUi('casio1.ui', self)
        self.firNumber = None
        self.typingNumber = False


        self.btn_9.clicked.connect(self.numberPressed)
        self.btn_8.clicked.connect(self.numberPressed)
        self.btn_7.clicked.connect(self.numberPressed)
        self.btn_6.clicked.connect(self.numberPressed)
        self.btn_5.clicked.connect(self.numberPressed)
        self.btn_4.clicked.connect(self.numberPressed)
        self.btn_3.clicked.connect(self.numberPressed)
        self.btn_2.clicked.connect(self.numberPressed)
        self.btn_1.clicked.connect(self.numberPressed)
        self.btn_0.clicked.connect(self.numberPressed)

        self.btn_cong.setCheckable(True)
        self.btn_tru.setCheckable(True)
        self.btn_nhan.setCheckable(True)
        self.btn_chia.setCheckable(True)
        self.btn_cong.clicked.connect(self.phep_toan)
        self.btn_tru.clicked.connect(self.phep_toan)
        self.btn_nhan.clicked.connect(self.phep_toan)
        self.btn_chia.clicked.connect(self.phep_toan)

        self.btn_cham.clicked.connect(self.dau_cham)
        self.btn_phantram.clicked.connect(self.unaryOperation)
        self.btn_congtru.clicked.connect(self.unaryOperation)
        
        self.btn_bang.clicked.connect(self.ketquapheptoan)
        self.btn_c.clicked.connect(self.clearbtn)

    def dau_cham(self):
         self.lbl_kq.setText(self.lbl_kq.text() + '.')

    def unaryOperation(self):
        button = self.sender()
        labelNumber = float(self.lbl_kq.text())
 
        if button.text() == "+/-":
            labelNumber = labelNumber * -1
        else:
            labelNumber = labelNumber * 0.01 
        newLabel = format(labelNumber, '.15g')
        self.lbl_kq.setText(newLabel)

    # nhấn có 2 trường hợp khi đã nhấn phép toán hoặc chưa nhấn
    # chưa nhấn phân 2 cái thập phân và không thập phân
    def numberPressed(self):
        button = self.sender()
        if ((self.btn_cong.isChecked() or 
             self.btn_tru.isChecked() or 
             self.btn_chia.isChecked() or 
             self.btn_nhan.isChecked()) 
             and (not self.typingNumber)):
            lbl = format(float(button.text()), '.15g') # ép kiểu dạng 64 bit
            self.typingNumber = True

        else:
 
            if (('.' in self.label.text()) and (button.text() == '0')):
                lbl = format(self.lbl_kq.text() + button.text(), '.15')
            else:
                lbl = format(float(self.lbl_kq.text() + button.text()), '.15g')
 
        self.lbl_kq.setText(lbl)

    def phep_toan(self):
        button = self.sender()
        self.firNumber = format(float(self.lbl_kq.text()),".15g")
        button.setChecked(True)
        self.label_2.setText(str(self.firNumber) + str(button.text()))

    def ketquapheptoan(self):
        # lấy nội dung số nhập 
        secondNumber = float(self.lbl_kq.text())
        self.label_2.clear()
        if(self.btn_cong.isChecked()):
            labelNumber = float(self.firNumber) + secondNumber
            newLabel = format(labelNumber, '.15g')
            self.lbl_kq.setText(newLabel)
            self.btn_cong.setChecked(False)
 
        elif (self.btn_tru.isChecked()):
            labelNumber = float(self.firNumber) - secondNumber
            newLabel = format(labelNumber, '.15g')
            self.lbl_kq.setText(newLabel)
            self.btn_tru.setChecked(False)
 
        elif (self.btn_nhan.isChecked()):
            labelNumber = float(self.firNumber) * secondNumber
            newLabel = format(labelNumber, '.15g')
            self.lbl_kq.setText(newLabel)
            self.btn_nhan.setChecked(False)
 
        elif (self.btn_chia.isChecked()):
            labelNumber = float(self.firNumber) / secondNumber
            newLabel = format(labelNumber, '.15g')
            self.lbl_kq.setText(newLabel)
            self.btn_chia.setChecked(False)
        self.typingNumber = False

    def clearbtn(self):
        self.btn_cong.setChecked(False)
        self.btn_tru.setChecked(False)
        self.btn_nhan.setChecked(False)
        self.btn_chia.setChecked(False)
        self.typingNumber = False
        self.lbl_kq.setText("0")
if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())
